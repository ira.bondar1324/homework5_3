import './App.css';
import Header from './Components/Header/Header';
import React, { useEffect, useState } from 'react';
import { saveStateToLocalStorage, getStateFromLocalStorage } from './utils/localStorageHelper';
import {CART_LS_KEY} from './constants';
import AppRoutes from "./AppRoutes";

function App() {

 const [productList, setProductList]=useState([]);
  const [countFavorite,setCountFavorite]=useState(0);
  const [modal,setModalState]=useState(false);
  const [modalCart,setModalCart]=useState(false);
  const [countCart,setCountCart]=useState(0);
  const [el,setEl]=useState({});
  const [favoritesEl,setFavoritesEl]=useState([]);
  const [cartElems,setCartElems]=useState([]);

  const favoritesOfEl=(el)=>{
    let favorArr;
    const findEl=favoritesEl.findIndex(elm=>el.id===elm.id);
    if(findEl===-1){
      setFavoritesEl((prev)=>{
        favorArr=[...prev];
        favorArr.push(el);
        saveStateToLocalStorage('favoritesEl',favorArr)
        return favorArr;
      })
    }else{
      setFavoritesEl((prev)=>{
        favorArr=[...prev];
        favorArr.splice(findEl,1);
        saveStateToLocalStorage('favoritesEl',favorArr)
        return favorArr;
      })
    }
  }

  const cartOfElms=(el)=>{
    let cartArr;
    const findEl=cartElems.findIndex(elm=>el.id===elm.id);
    if(el.cart){
      if(findEl===-1){
        setCartElems((prev)=>{
          cartArr=[...prev];
          cartArr.push(el);
          saveStateToLocalStorage('cartElms',cartArr);
          return cartArr;
        })
      }else{
        saveStateToLocalStorage('cartElms',cartElems);
      }}else{
      setCartElems((prev)=>{
        cartArr=[...prev];
        cartArr.splice(findEl,1);
        saveStateToLocalStorage('cartElms',cartArr);
        return cartArr;
      })
    }
  }


  const changeCountElement=(el)=>{
    if(el.cart){
      el.countInCart=el.countInCart+1;
      saveStateToLocalStorage(CART_LS_KEY,productList);
    }else{
      el.countInCart=0;
      saveStateToLocalStorage(CART_LS_KEY,productList);
    }

  }

   const changeEl=(el)=>{
    setEl(el);
  }

  function changeStateModal(type){
    if(type==="cart"){
      if(modalCart){
        setModalCart(false)
      }else{
        setModalCart(true)
      }
    }else
      if(modal){
        setModalState(false)
      }else{
        setModalState(true)
      }
  }

  const changeCountCart=(el,countElement)=>{
    let count
      setCountCart((prev)=>{
        count=prev;
        if(el.cart){
          count++;
        }else{
          count=count-countElement;
        }
        saveStateToLocalStorage('countCart',count);
        return count;
      })
    }

  const changeFavoriteOfElement=(el)=>{
    let counter;
    if(el.favorite){
      el.favorite=false;
      setCountFavorite((prev)=>{
        counter=prev;
        counter--;
        saveStateToLocalStorage('countFavorite',counter);
        return counter;
      })
    }else{
      el.favorite=true;
      setCountFavorite((prev)=>{
        counter=prev;
        counter++;
        saveStateToLocalStorage('countFavorite',counter);
        return counter;
      })
    }
    saveStateToLocalStorage(CART_LS_KEY,productList);
  }


  const getItems = async()=>{
    try{
      const data=await fetch("./products.json").then(res=>res.json());
      setProductList(data.products);
      saveStateToLocalStorage(CART_LS_KEY,data.products)
    }catch(e){
      console.log(e.message);
    }
  }

  useEffect(()=>{
    const cartFromLs=getStateFromLocalStorage(CART_LS_KEY);
    if(cartFromLs){
      setProductList(cartFromLs);
    }else{
      getItems();
    }

    const countFavoriteEl =getStateFromLocalStorage("countFavorite");
    if(countFavoriteEl){
      setCountFavorite(countFavoriteEl);
    }

    const countCartEl =getStateFromLocalStorage("countCart");
    if(countCartEl){
      setCountCart(countCartEl);
    }

    const favoritesElements=getStateFromLocalStorage('favoritesEl');
    if(favoritesElements){
      setFavoritesEl(favoritesElements);
    }

    const cartElements=getStateFromLocalStorage('cartElems');
    if(cartElements){
      setCartElems(cartElements);
    }

  },[])

  return (
    <>
      <Header countCart={countCart} countFavorite={countFavorite} />
      <AppRoutes cartElems={cartElems} cartOfElms={cartOfElms} productList={productList} 
      favoritesEl={favoritesEl} 
      favoritesOfEl={favoritesOfEl} 
      changeEl={changeEl} 
      changeFavoriteOfElement={changeFavoriteOfElement} 
      el={el} 
      changeCountElement={changeCountElement} 
      changeCountCart={changeCountCart} 
      changeStateModal={changeStateModal} 
      isOpen={modal}/>
    </>
  );
}

export default App;
