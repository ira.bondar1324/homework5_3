import React from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";

const Button = ({favoritesOfEl,changeEl,children,backgroundColor,changeStateModal,el,changeFavoriteOfElement,openBtn,changeColorHeart}) => {

    
    const helper=()=>{
        if(openBtn==="modal"){
            changeStateModal();
            changeEl(el);
        }else{
            favoritesOfEl(el);
            changeFavoriteOfElement(el);
            changeColorHeart();
        }
    }
    return(
            <button onClick={helper}className={styles.button} style={{backgroundColor:backgroundColor}}>{children}</button>
    );
}

Button.propTypes={
    favoritesOfEl:PropTypes.func,
    changeEl:PropTypes.func,
    children:PropTypes.oneOfType([PropTypes.string,PropTypes.object]),
    backgroundColor:PropTypes.string,
    changeStateModal:PropTypes.func,
    el:PropTypes.object,
    changeFavoriteOfElement:PropTypes.func,
    openBtn:PropTypes.string,
    changeColorHeart:PropTypes.func
}

Button.defaultProps={
    favoritesOfEl:()=>{},
    changeEl:()=>{},
    children:"",
    backgroundColor:"",
    changeStateModal:()=>{},
    el:{},
    changeFavoriteOfElement:()=>{},
    openBtn:"",
    changeColorHeart:()=>{}
}

export default Button;