import React from "react";
import ProductItem from "../ProductItem/ProductItem";
import styles from "./ProductList.module.scss";
import PropTypes from "prop-types";

const ProductList=({type,favoritesOfEl,changeEl,productList,changeFavoriteOfElement,changeStateModal})=>{
return(
    <ul className={styles.productList}>
        {productList.map(el=>
        <ProductItem type={type} favoritesOfEl={favoritesOfEl} changeEl={changeEl} changeStateModal={changeStateModal} changeFavoriteOfElement={changeFavoriteOfElement} key={el.id} el={el}/>)}
    </ul>
);
}

ProductList.propTypes={
    type:PropTypes.string,
    favoritesOfEl:PropTypes.func,
    changeEl:PropTypes.func,
    productList:PropTypes.array.isRequired,
    changeFavoriteOfElement:PropTypes.func,
    changeStateModal:PropTypes.func
}

ProductList.defaultProps={
    type:"",
    favoritesOfEl:()=>{},
    changeEl:()=>{},
    productList:[],
    changeFavoriteOfElement:()=>{},
    changeStateModal:()=>{}
}

export default ProductList;