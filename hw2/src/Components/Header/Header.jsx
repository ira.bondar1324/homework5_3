import React from "react";
import styles from "./Header.module.scss";
import PropTypes from "prop-types";
import {NavLink } from "react-router-dom";

const Header=({countFavorite, countCart})=>{
return(
    <>
    <header className={styles.headerWrapper}>  

        <NavLink to='/'>
            <img src="https://timeshop.com.ua/images/ab__webp/logos/169/timeshop-logo-1_png.webp" alt="Timeshop" />
        </NavLink>
        <ul className={styles.headerWrapperIcons}>

            <li className={styles.headerWrapperItem}>
                <NavLink to="/favorite" className={styles.headerWrapperIconsItem}>
                    <p className={styles.headerIconText}>Favorite</p>
                    <p className={styles.headerIconText}>{countFavorite}</p>
                </NavLink>
            </li>

            <li className={styles.headerWrapperItem}>
                <NavLink to="/cart" className={styles.headerWrapperIconsItem}>
                    <p className={styles.headerIconText}>Cart</p>
                    <p className={styles.headerIconText}>{countCart}</p>
                </NavLink>
            </li>
        </ul>
    </header>

    </>
    )
}

Header.propTypes={
    countFavorite:PropTypes.number,
    countCart:PropTypes.number
}

Header.defaultProps={
    countFavorite:0,
    countCart:0
}

export default Header;