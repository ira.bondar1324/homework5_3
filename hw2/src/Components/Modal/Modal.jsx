import React from "react";
import styles from "./Modal.module.scss";
import PropTypes from "prop-types";


const Modal = ({cartOfElms,changeCountElement,el,header,text,isOpen,changeStateModal,changeCountCart,type}) =>{

    let displayIs;
    isOpen?displayIs={display:"flex"}:displayIs={display:"none"};

    const click = (e)=>{
        if(e.target.className==="Modal_modalWindowHeaderCross__j+Bo8"||e.target.className==="Modal_modalWrapper__vp8fB"){
            changeStateModal();
        }
    }
    
    function modalHome(el){
        el.cart=true;
        changeStateModal();
        changeCountCart(el,el.countInCart);
        changeCountElement(el);
        cartOfElms(el);
    }

    function modalCartDelete(el){
        el.cart=false;
        changeStateModal();
        changeCountCart(el,el.countInCart);
        changeCountElement(el);
        cartOfElms(el);
    }

    return(
        <div onClick={click} style={displayIs} className={styles.modalWrapper}>
            <div className={styles.modalWindow}>
                <header className={styles.modalWindowHeader}>
                    <h2 className={styles.modalWindowHeaderText}>{header}</h2>
                    <p className={styles.modalWindowHeaderCross}>X</p>
                </header>
                <main className={styles.modalWindowMain}>
                    <p className={styles.modalWindowMainText}>{text}</p>
                    <div className={styles.modalMainBtns}>
                        <button className={styles.modalBtn} onClick={()=>{type==="home"?modalHome(el):modalCartDelete(el)}}>Yes</button>
                        <button onClick={changeStateModal} className={styles.modalBtn}>No</button>
                    </div>
                </main>
            </div>
        </div>
    );

}

Modal.propTypes={
    type:PropTypes.string,
    cartOfElms:PropTypes.func,
    changeCountElement:PropTypes.func,
    el:PropTypes.object,
    header:PropTypes.string,
    text:PropTypes.string,
    isOpen:PropTypes.bool,
    changeStateModal:PropTypes.func,
    changeCountCart:PropTypes.func
}

Modal.defaultProps={
    type:"",
    cartOfElms:()=>{},
    changeCountElement:()=>{},
    el:{},
    header:"",
    text:"",
    isOpen:false,
    changeStateModal:()=>{},
    changeCountCart:()=>{}
}

export default Modal;