import React from "react";
import ProductList from "../../Components/ProductList/ProductList";
import Modal from "../../Components/Modal/Modal";
import PropTypes from "prop-types";

const HomePage=({cartOfElms,productList,favoritesEl, favoritesOfEl, changeEl, changeStateModal, changeFavoriteOfElement,el,changeCountElement,changeCountCart,isOpen})=>{
    return(
        <>
            <ProductList favoritesEl={favoritesEl} favoritesOfEl={favoritesOfEl} changeEl={changeEl} changeStateModal={changeStateModal} changeFavoriteOfElement={changeFavoriteOfElement} productList={productList} />
            <Modal type='home' cartOfElms={cartOfElms} el={el} changeCountElement={changeCountElement} changeCountCart={changeCountCart} changeStateModal={changeStateModal} header="Add product to cart" text="Do you want to add this product is cart?" isOpen={isOpen}/>
        </>
    );
}

HomePage.propTypes={
    favoritesOfEl:PropTypes.func,
    changeEl:PropTypes.func,
    productList:PropTypes.array.isRequired,
    changeFavoriteOfElement:PropTypes.func,
    changeStateModal:PropTypes.func,

    cartOfElms:PropTypes.func,
    changeCountElement:PropTypes.func,
    el:PropTypes.object,
    isOpen:PropTypes.bool,
    changeCountCart:PropTypes.func,

    favoritesEl:PropTypes.array
}

HomePage.defaultProps={
    favoritesOfEl:()=>{},
    changeEl:()=>{},
    productList:[],
    changeFavoriteOfElement:()=>{},
    changeStateModal:()=>{},
    cartOfElms:()=>{},
    changeCountElement:()=>{},
    el:{},
    isOpen:false,
    changeCountCart:()=>{},
    favoritesEl:[]
}

export default HomePage;