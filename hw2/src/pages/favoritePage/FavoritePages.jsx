import React from "react";
import ProductList from "../../Components/ProductList/ProductList";
import PropTypes from "prop-types";

const FavoritePages=({favoritesOfEl,changeEl,productList,changeFavoriteOfElement,changeStateModal})=>{

    const favoriteProducts=productList.filter(el=>el.favorite===true);
    return(
        <>
            <ProductList productList={favoriteProducts}
            favoritesOfEl={favoritesOfEl}
            changeEl={changeEl}
            changeFavoriteOfElement={changeFavoriteOfElement}
            changeStateModal={changeStateModal}/>
        </>
    );
}

FavoritePages.propTypes={
    favoritesOfEl:PropTypes.func,
    changeEl:PropTypes.func,
    productList:PropTypes.array.isRequired,
    changeFavoriteOfElement:PropTypes.func,
    changeStateModal:PropTypes.func,
}

FavoritePages.defaultProps={
    favoritesOfEl:()=>{},
    changeEl:()=>{},
    productList:[],
    changeFavoriteOfElement:()=>{},
    changeStateModal:()=>{},
}

export default FavoritePages;