import React from "react";
import ProductList from "../../Components/ProductList/ProductList";
import Modal from "../../Components/Modal/Modal";
import PropTypes from "prop-types";

const CartPages=({favoritesOfEl,changeEl,productList,changeFavoriteOfElement,changeStateModal,cartOfElms,
    changeCountElement, 
    el,
    isOpen,
    changeCountCart})=>{

    const cartProducts=productList.filter(el=>el.cart===true);

    return(
        <>
            <ProductList type='cart' productList={cartProducts}
                favoritesOfEl={favoritesOfEl}
                changeEl={changeEl}
                changeFavoriteOfElement={changeFavoriteOfElement}
                changeStateModal={changeStateModal}/>
            <Modal type='delete' cartOfElms={cartOfElms} 
                changeCountElement={changeCountElement}
                el={el}
                isOpen={isOpen}
                changeStateModal={changeStateModal}
                changeCountCart={changeCountCart}
                header="Delete product from cart"
                text="Do you want to delete this product from cart?"
                />
        </>
    );
}

CartPages.propTypes={
    favoritesOfEl:PropTypes.func,
    changeEl:PropTypes.func,
    productList:PropTypes.array.isRequired,
    changeFavoriteOfElement:PropTypes.func,
    changeStateModal:PropTypes.func,
    cartOfElms:PropTypes.func,
    changeCountElement:PropTypes.func,
    el:PropTypes.object,
    isOpen:PropTypes.bool,
    changeCountCart:PropTypes.func
}

CartPages.defaultProps={
    favoritesOfEl:()=>{},
    changeEl:()=>{},
    productList:[],
    changeFavoriteOfElement:()=>{},
    changeStateModal:()=>{},
    cartOfElms:()=>{},
    changeCountElement:()=>{},
    el:{},
    isOpen:false,
    changeCountCart:()=>{}
}

export default CartPages;