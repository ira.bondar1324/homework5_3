import React from "react";
import HomePage from "./pages/homePage/HomePage";
import FavoritePages from "./pages/favoritePage/FavoritePages";
import CartPages from "./pages/cartPage/CartPages";
import { Routes,Route} from "react-router-dom";
import PropTypes from "prop-types";

const AppRoutes=({productList,
    favoritesEl,
    favoritesOfEl,
    changeEl,
    changeFavoriteOfElement,
    el,
    changeCountElement,
    changeCountCart,
    changeStateModal,
    isOpen,
    cartOfElms})=>{
    return(
        <Routes>
            <Route path="/" element={<HomePage 
                productList={productList}
                favoritesEl={favoritesEl}
                favoritesOfEl={favoritesOfEl}
                changeEl={changeEl}
                changeStateModa={changeStateModal}
                changeFavoriteOfElement={changeFavoriteOfElement}
                el={el} 
                changeCountElement={changeCountElement} 
                changeCountCart={changeCountCart} 
                changeStateModal={changeStateModal} 
                isOpen={isOpen} 
                cartOfElms={cartOfElms}/>}>  
            </Route>

            <Route path="/favorite" element={
                <FavoritePages productList={productList}
                    favoritesOfEl={favoritesOfEl}
                    changeEl={changeEl}
                    changeFavoriteOfElement={changeFavoriteOfElement}
                    changeStateModal={changeStateModal}/>}
                >
            </Route>
            <Route path="/cart" element={
                <CartPages productList={productList} 
                    favoritesOfEl={favoritesOfEl}
                    changeEl={changeEl}
                    changeFavoriteOfElement={changeFavoriteOfElement}
                    changeStateModal={changeStateModal}
                    cartOfElms={cartOfElms}
                    changeCountElement={changeCountElement} 
                    el={el}
                    isOpen={isOpen}
                    changeCountCart={changeCountCart} />}
                >
            </Route>
        </Routes>
    );
}

AppRoutes.propTypes={
    favoritesOfEl:PropTypes.func,
    changeEl:PropTypes.func,
    productList:PropTypes.array.isRequired,
    changeFavoriteOfElement:PropTypes.func,
    changeStateModal:PropTypes.func,

    cartOfElms:PropTypes.func,
    changeCountElement:PropTypes.func,
    el:PropTypes.object,
    isOpen:PropTypes.bool,
    changeCountCart:PropTypes.func,

    favoritesEl:PropTypes.array
}

AppRoutes.defaultProps={
    favoritesOfEl:()=>{},
    changeEl:()=>{},
    productList:[],
    changeFavoriteOfElement:()=>{},
    changeStateModal:()=>{},
    cartOfElms:()=>{},
    changeCountElement:()=>{},
    el:{},
    isOpen:false,
    changeCountCart:()=>{},
    favoritesEl:[]
}
export default AppRoutes;